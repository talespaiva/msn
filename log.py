import logging
from logging.handlers import RotatingFileHandler
from datetime import datetime


def get_logger():
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    if not logger.handlers:
        file_handler = RotatingFileHandler(f'{datetime.now()}.log', maxBytes=(1048576 * 5), backupCount=7)
        file_handler.setLevel(logging.INFO)

        log_handler = logging.StreamHandler()
        log_handler.setLevel(logging.INFO)

        formatter = logging.Formatter('%(asctime)s - %(module)s - %(levelname)s >> %(message)s')

        log_handler.setFormatter(formatter)
        logger.addHandler(log_handler)

        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)

    return logger
