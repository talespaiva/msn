import os
import sys
sys.path.insert(0, os.path.pardir)

from msn import preprocessing as pp
import log

import pandas as pd
import numpy as np
from tqdm import tqdm

LOGGER = log.get_logger()

ROOT_PATH = os.path.expanduser('~/Downloads/DCC_DublinBusGPSSample_P20130415-0916/')
METADATA_PATH = 'metadata/'


def create_metadata_file(file_name):

    column_names = [
        'timestamp', 'line_id', 'direction', 'journey_pattern_id', 'time_frame',
        'vehicle_journey_id', 'operator', 'congestion', 'lon', 'lat', 'delay', 'block_id',
        'vehicle_id', 'stop_id', 'at_stop'
    ]

    df = pd.read_csv(ROOT_PATH + file_name, header=0, names=column_names)

    df['timestamp_posix'] = df.timestamp.astype(int)
    df['timestamp'] = pd.to_datetime(df.timestamp, unit='us')
    df.vehicle_journey_id = df.vehicle_journey_id.astype(int)

    # convert all line_id's to string
    line_id_int = []
    for entry in df.line_id:
        try:
            line_id_int.append(str(int(entry)))
        except:
            line_id_int.append(entry)

    df.line_id = line_id_int

    runs = df.groupby(['vehicle_journey_id', 'line_id'])

    # Compute some metadata about each trajectory (run)
    sample_rates = []
    periods = []
    durations = []
    n_points = []
    n_stops = []
    n_congestion = []
    distance_std_deviations = []
    total_distances = []
    total_durations = []
    metadata_index = []

    for run in tqdm(runs):
        run[1].drop_duplicates(subset=['lon', 'lat'], keep='last', inplace=True)

        period = 0
        sample_rate = 0

        duration = (run[1].timestamp.iloc[-1] - run[1].timestamp.iloc[0]).seconds

        if duration > 0:
            sample_rate = len(run[1]) / duration
            period = 1 / sample_rate

        n_stops.append(len(run[1][run[1]['at_stop'] == 1]))
        n_congestion.append(len(run[1][run[1]['congestion'] == 1]))

        gpx = pp.dublin_bus_to_gpx(run[1])
        df_msn = pp.compute_attributes(gpx)
        distance_std_deviations.append(np.std(df_msn.distance))
        total_distances.append(sum(df_msn.distance))
        total_durations.append(sum(df_msn.duration))

        sample_rates.append(sample_rate)
        periods.append(period)
        durations.append(duration)
        n_points.append(len(run[1]))
        metadata_index.append(run[0])

    metadata = pd.DataFrame(index=metadata_index)
    metadata['sample_rate'] = sample_rates
    metadata['period'] = periods
    metadata['duration'] = durations
    metadata['n_points'] = n_points
    metadata['n_stops'] = n_stops
    metadata['n_congestion'] = n_congestion
    metadata['std_distance'] = distance_std_deviations
    metadata['distance'] = total_distances
    metadata['duration'] = total_durations

    metadata.to_csv(ROOT_PATH + METADATA_PATH + file_name)


if __name__ == '__main__':
    with os.scandir(ROOT_PATH) as it:
        for entry in it:
            if entry.is_file() and entry.name.endswith('.csv'):
                if not os.path.exists(ROOT_PATH + METADATA_PATH + entry.name):
                    LOGGER.info('Starting %s', entry.name)
                    create_metadata_file(entry.name)
                else:
                    LOGGER.info('Already exists: %s', ROOT_PATH + METADATA_PATH + entry.name)
