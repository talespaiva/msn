import os
import sys
sys.path.insert(0, os.path.pardir)

from msn import msn
from msn import preprocessing as pp
import log

import time
import pandas as pd
import numpy as np
import overpy
from geopy.distance import distance
from tqdm import tqdm

LOGGER = log.get_logger()

osm = overpy.Overpass()

ROOT_PATH = os.path.expanduser('~/Downloads/DCC_DublinBusGPSSample_P20130415-0916/')
METADATA_PATH = 'metadata/'
RESULTS_PATH = 'results/'


def select_runs(metadata_folder_path):
    selected = {}

    with os.scandir(metadata_folder_path) as it:
        for entry in it:
            if entry.is_file() and entry.name.endswith('.csv'):
                metadata = pd.read_csv(
                    metadata_folder_path + entry.name,
                    header=0,
                    names=[
                        'index', 'sample_rate', 'period', 'duration', 'n_points', 'n_stops',
                        'n_congestion', 'std_distance', 'distance'
                    ])

                indexes = metadata[(metadata.n_points > 80) &
                                   (metadata.std_distance <= 60) &
                                   (metadata.duration < 12000)]['index']

                selected.update({
                    entry.name: [eval(i) for i in indexes]
                })

    return selected


def query_osm(query):
    try:
        return osm.query(query)
    except overpy.exception.OverpassTooManyRequests as tmr:
        LOGGER.info(tmr)
        LOGGER.info('Sleeping...')
        time.sleep(30)
        query_osm(query)


def apply_msn(file_name, selected_runs):

    if os.path.exists(ROOT_PATH + RESULTS_PATH + file_name):
        LOGGER.info('Result file already exists.')
        return None

    column_names = [
        'timestamp', 'line_id', 'direction', 'journey_pattern_id', 'time_frame',
        'vehicle_journey_id', 'operator', 'congestion', 'lon', 'lat', 'delay', 'block_id',
        'vehicle_id', 'stop_id', 'at_stop'
    ]

    df = pd.read_csv(ROOT_PATH + file_name, header=0, names=column_names)

    df['timestamp_posix'] = df.timestamp.astype(int)
    df['timestamp'] = pd.to_datetime(df.timestamp, unit='us')
    df.vehicle_journey_id = df.vehicle_journey_id.astype(int)

    # convert all line_id's to string
    line_id_int = []
    for entry in df.line_id:
        try:
            line_id_int.append(str(int(entry)))
        except:
            line_id_int.append(entry)

    df.line_id = line_id_int

    runs = df.groupby(['vehicle_journey_id', 'line_id'])

    df_result = pd.DataFrame()

    for group_index in tqdm(selected_runs):
        LOGGER.info('\n')
        LOGGER.info(f'{group_index}')

        run = runs.get_group(group_index)
        run.set_index('timestamp', inplace=True)
        run['timestamp'] = run.index

        run.drop_duplicates(subset=['lon', 'lat'], keep='last', inplace=True)

        # MSN
        gpx = pp.dublin_bus_to_gpx(run)
        df_msn = pp.compute_attributes(gpx)

        LOGGER.info(f'Total distance = {df_msn.distance.sum()} meters.')
        LOGGER.info(f'Number of points = {len(df_msn)+1}.')

        m, s, n = msn.get_move_stop_noise(
            df_msn,
            distance_threshold=3.5,
            direction_threshold=45,
            speed_threshold=3.5,
            time_threshold=5)

        if len(s) == 0:
            LOGGER.info('No stop detected, skipping...')
            continue

        stop_points = np.array(s)[:, 0]
        stops = df_msn.iloc[stop_points]

        LOGGER.info(f'{len(stop_points)} stops detected.')

        # OSM
        radius = 30

        counter = 0
        stops_detail = {}

        for i, point in enumerate(list(zip(stops.latitude,
                                           stops.longitude,
                                           stops.index,
                                           stops.speed,
                                           stops.duration))):

            coords = [point[0], point[1]]

            LOGGER.info(f'Stop {i+1}: {coords}')

            query = f'node(around:{radius},{coords[0]},{coords[1]});(._;>;);out body;'

            result = query_osm(query)

            counted = False
            bus_stop = 0
            traffic_signals = 0
            other = 0
            highway_values = []
            for node in result.nodes:
                if 'highway' in node.tags.keys():
                    highway_value = node.tags['highway']
                    
                    # do not count features in the list below
                    if highway_value in ['street_lamp', 'street_cabinet',
                                         'city_junction', 'elevator',
                                         'speed_camera', 'milestone']:
                        LOGGER.info('Skipping highway tag value: %s', highway_value)
                        continue

                    dist = distance([node.lat, node.lon], coords)
                    LOGGER.info(f'{node.tags["highway"]}\t{dist.meters:.2f}\t{float(node.lat), float(node.lon)}')

                    if highway_value == 'bus_stop' or highway_value == 'stop':
                        bus_stop += 1
                    elif highway_value == 'traffic_signals' or (
                            highway_value == 'crossing' and
                            'crossing' in node.tags and
                            node.tags['crossing'] == 'traffic_signals'):
                        traffic_signals += 1
                    else:
                        other += 1
                        LOGGER.info(node.tags)

                    if not counted:
                        counter += 1
                        counted = True

                    highway_values.append(highway_value)

            stops_detail.update({
                f'stop-{i + 1}': {
                    'location': coords,
                    'duration': point[4],
                    'speed': point[3],
                    'bus': bus_stop,
                    'signal': traffic_signals,
                    'other': other,
                    'highway_values': highway_values
                }
            })

        hit_ratio = 0
        if counter > 0:
            hit_ratio = counter / len(stop_points)

        df_result = df_result.append({'id': group_index,
                                      'msn_stops': len(stop_points),
                                      'osm_stops': counter,
                                      'hit_ratio': hit_ratio,
                                      'details': str(stops_detail)
                                      }, ignore_index=True)

    df_result = df_result[['id', 'msn_stops', 'osm_stops', 'hit_ratio', 'details']]
    df_result.to_csv(ROOT_PATH + RESULTS_PATH + file_name)


if __name__ == '__main__':

    selected_runs = select_runs(ROOT_PATH + METADATA_PATH)

    total = 0
    for item in selected_runs.items():
        LOGGER.info(f'{item[0]} {(item[1])}')
        total += len(item[1])

    LOGGER.info(total)

    for selection in selected_runs.items():
        file_name = selection[0]
        run_ids = selection[1]

        LOGGER.info('Starting %s', file_name)
        LOGGER.info('Number of selected runs: %s', len(run_ids))

        apply_msn(file_name, run_ids)

    LOGGER.info('Finished.')
